# Face Detection Models

## Requirements

It requires Python 3.6 to run our system

Development for this project will be isolated in Python virtual environment. This allows us to experiment with different versions of dependencies.

There are many ways to install `virtual environment (virtualenv)`, see the [Python Virtual Environments: A Primer](https://realpython.com/python-virtual-environments-a-primer/) guide for different platforms:

```bash
$ pip install virtualenv
```

## Detail Guide

### Deep learning based Face detection using the YOLOv3 algorithm and Face recognition using the FaceNet algorithm

#### Get started

The YOLOv3 (You Only Look Once) is a state-of-the-art, real-time object detection algorithm. The published model recognizes 80 different objects in images and videos. For more details, you can refer to this paper.

#### Yolov3 Architecture

![](./wild_face/Architecture/yolov3.png)


#### FaceNet Architecture

![](./wild_face/Architecture/facenet.png)

### Comparison to Other Detectors

YOLOv3 is extremely fast and accurate. In mAP measured at .5 IOU YOLOv3 is on par with Focal Loss but about 4x faster. Moreover, you can easily tradeoff between speed and accuracy simply by changing the size of the model, no retraining required!

![](./wild_face/Architecture/compare.png)

### Performance on the COCO Dataset

![](./wild_face/Architecture/perform.png)


### How to run

1. Clone this repository
```bash
$ git clone https://gitlab.com/Student-Rollup/student_rollup.git
$ cd FRA
```

2. Create a Python 3.6 virtual environment for this project and activate the 
virtualenv:
```bash
$ virtualenv -p python3.6 venv
$ source ./venv/bin/activate
```

3. Next, install the dependencies for the this project:
```bash
$ pip install -r requirements.txt
```

4. Run the following command to test face detector:
```bash
$ python wild_f/tests/test_wide_face_detector.py [--input] [--method]
```
