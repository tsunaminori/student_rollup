import glob
import os
import tensorflow as tf
import cv2
import dlib
import numpy as np
import pandas as pd

from sklearn.naive_bayes import MultinomialNB
from scipy.spatial import distance
from tqdm import tqdm
from wild_face.face_detector import YoloFaceDetector
from wild_face.networks.facenet.align import AlignDlib
from wild_face.networks.facenet.inception_v2 import create_model


# Config path
path = os.path.dirname(__file__)


class FaceNetOneShotRecognitor(object):
    def __init__(self, path_weight, path_landmask, path_train):
        self.path_weight = path_weight
        self.path_landmask = path_landmask

        # INITIALIZE MODELS
        model = create_model()
        model.summary()
        model.load_weights(self.path_weight)
        self.graph = tf.get_default_graph()
        self.model = model
        self.alignment = AlignDlib(self.path_landmask)

        # LOAD TRAINING INFORMATION
        self.train_paths = glob.glob(path_train)
        self.nb_classes = len(self.train_paths)
        self.df_train = pd.DataFrame(columns=['image', 'label', 'name'])

    def __align_face(self, face):
        # print(img.shape)
        (h, w, c) = face.shape
        bb = dlib.rectangle(0, 0, w, h)
        # print(bb)
        return self.alignment.align(96, face, bb, landmarkIndices=AlignDlib.OUTER_EYES_AND_NOSE)

    def __l2_normalize(self, x, axis=-1, epsilon=1e-10):
        output = x / np.sqrt(np.maximum(np.sum(np.square(x), axis=axis, keepdims=True), epsilon))
        return output

    def __load_and_align_images(self, filepaths):
        aligned_images = []
        for filepath in filepaths:
            # print(filepath)
            img = cv2.imread(filepath)
            aligned = self.__align_face(img)
            aligned = (aligned / 255.).astype(np.float32)
            aligned = np.expand_dims(aligned, axis=0)
            aligned_images.append(aligned)

        return np.array(aligned_images)

    def __align_faces(self, faces):
        aligned_images = []
        for face in faces:
            # print(face.shape)
            aligned = self.__align_face(face)
            aligned = (aligned / 255.).astype(np.float32)
            aligned = np.expand_dims(aligned, axis=0)
            aligned_images.append(aligned)

        return aligned_images

    def __calc_embs(self, filepaths, batch_size=128):
        pd = []
        for start in tqdm(range(0, len(filepaths), batch_size)):
            aligned_images = self.__load_and_align_images(filepaths[start:start + batch_size])
            pd.append(self.model.predict_on_batch(np.squeeze(aligned_images)))
        # embs = self.__l2_normalize(np.concatenate(pd))
        embs = np.array(pd)

        return np.array(embs)

    def __calc_emb_test(self, faces):
        pd = []
        aligned_faces = self.__align_faces(faces)
        if (len(faces) == 1):
            with self.graph.as_default():
                pd.append(self.model.predict_on_batch(aligned_faces))
        elif (len(faces) > 1):
            with self.graph.as_default():
                pd.append(self.model.predict_on_batch(np.squeeze(aligned_faces)))
        # embs = self.__l2_normalize(np.concatenate(pd))
        embs = np.array(pd)
        return np.array(embs)

    def train_or_load(self, batch_size, cons=True):

        for i, train_path in enumerate(self.train_paths):
            name = train_path.split("\\")[-1]
            images = glob.glob(train_path + "/*")
            for image in images:
                self.df_train.loc[len(self.df_train)] = [image, i, name]

        # TRAINING
        label2idx = []
        for i in tqdm(range(len(self.train_paths))):
            label2idx.append(np.asarray(self.df_train[self.df_train.label == i].index))
        if cons:
            train_embs = self.__calc_embs(self.df_train.image, batch_size)
            np.save(path + "/weights/facenet/train_embs_v1.npy", train_embs)
        else:
            train_embs = np.load(path + '/weights/facenet/train_embs_v1.npy')
        train_embs = np.concatenate(train_embs)

        return train_embs, label2idx

    def __median(self, cluster, size):
        s = np.zeros(128)
        for i in range(size):
            s+= cluster[i].reshape(-1)
        return s/size

    def __navies_bayes(self,train_data, label, test):
        # call MultinomialNB
        clf = MultinomialNB()
        clf.fit(train_data, label)
        return clf.predict(test)[0]

    def predict(self, faces, train_embs, label2idx, threshold=.4):
        test_embs = self.__calc_emb_test(faces)
        test_embs = np.concatenate(test_embs)
        people = []
        unknowns = []
        print(train_embs)
        for i in range(test_embs.shape[0]):
            distances = []
            for j in range(len(self.train_paths)):
                # the min of clustering
                distances.append(
                    np.min([distance.euclidean(test_embs[i].reshape(-1), train_embs[k].reshape(-1)) for k in
                            label2idx[j]]))

            # Navies Bayse
            # label = np.array(list(map(str, self.df_train["label"])))
            # for _ in test_embs:
            #     distances.append(self.__navies_bayes(abs(train_embs), label, np.array([_])))

            if np.min(distances) > threshold:
                unknowns.append(faces[i])
            else:
                res = np.argsort(distances)[:1]
                people.append(res)
        names = []
        if len(people) > 0:
            for p in people:
                name = self.df_train[(self.df_train['label'] == p[0])].name.iloc[0]
                name = name.split("/")[-1]
                names.append(name)

        return names, unknowns

    def analysing(self, train_embs, nb_classes, label2idx, ):
        import matplotlib.pyplot as plt

        match_distances = []
        for i in range(nb_classes):
            ids = label2idx[i]
            distances = []
            for j in range(len(ids) - 1):
                for k in range(j + 1, len(ids)):
                    distances.append(distance.euclidean(train_embs[ids[j]].reshape(-1), train_embs[ids[k]].reshape(-1)))
            match_distances.extend(distances)

        unmatch_distances = []
        for i in range(nb_classes):
            ids = label2idx[i]
            distances = []
            for j in range(10):
                idx = np.random.randint(train_embs.shape[0])
                while idx in label2idx[i]:
                    idx = np.random.randint(train_embs.shape[0])
                distances.append(
                    distance.euclidean(train_embs[ids[np.random.randint(len(ids))]].reshape(-1),
                                       train_embs[idx].reshape(-1)))
            unmatch_distances.extend(distances)

        _, _, _ = plt.hist(match_distances, bins=100)
        _, _, _ = plt.hist(unmatch_distances, bins=100, fc=(1, 0, 0, 0.5))

        plt.show()

#
if __name__ == '__main__':

    img = cv2.imread(path+'/tests/images/img2.jpg')
    model = FaceNetOneShotRecognitor(path_weight='./weights/facenet/nn4.small2.v1.h5',
                                     path_landmask='./weights/facenet/shape_predictor_68_face_landmarks.dat',
                                     path_train='./dataset/*')

    # Init face_detection model
    face_detection_model = YoloFaceDetector(model_path=path + "/weights/yolov3/model.ckpt",
                                            iou_threshold=0.5,
                                            confidence_threshold=0.5)

    train_embs, label2idx = model.train_or_load(batch_size=128,
                                                cons=True)

    face_locations = face_detection_model.detect_face_by_image(im=img)
    faces = []
    for face_location in face_locations:
        x1, y1, x2, y2 = face_location[0]
        face = img[y1:y2, x1:x2]
        face = cv2.resize(face, (96, 96))
        faces.append(face)

    face_names, unknown = model.predict(faces=faces,
                                        train_embs=train_embs,
                                        label2idx=label2idx)

    print(face_names)
    for i in unknown:
        cv2.imshow("im", np.squeeze(i))
        cv2.waitKey(0)
#     # # TEST
#     # test_paths = glob.glob("./tests/images/test1.jpg")
#     # faces = []
#     # for path in test_paths:
#     #     test_image = cv2.imread(path)
#     #     show_image = test_image.copy()
#     #
#     #     hogFaceDetector = dlib.get_frontal_face_detector()
#     #     faceRects = hogFaceDetector(test_image, 0)
#     #     for faceRect in faceRects:
#     #         x1 = faceRect.left()
#     #         y1 = faceRect.top()
#     #         x2 = faceRect.right()
#     #         y2 = faceRect.bottom()
#     #         face = test_image[y1:y2, x1:x2]
#     #
#     #         faces.append(face)
#     #
#     # names, _ = model.predict(faces=faces,
#     #                          train_embs=train_embs,
#     #                          label2idx=label2idx)
#     # print(names)
#     # for i in _:
#     #     cv2.imshow("im", np.squeeze(i))
#     #     cv2.waitKey(0)
#     # cv2.destroyAllWindows()
