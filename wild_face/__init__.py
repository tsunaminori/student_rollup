from __future__ import absolute_import, division, print_function

__all__ = [
    "__title__", "__summary__", "__uri__", "__version__", "__author__",
    "__email__", "__license__", "__copyright__",
]

__title__ = "wild_face"
__summary__ = ("wild_face  is a package that provides detection & recognition face"
               " and primitives to Python developers.")
__uri__ = "https://gitlab.com/Student-Rollup/student_rollup.git"

__version__ = "1.0.0"

__author__ = "Duong pham"
__email__ = "phduong1997s@gmail.com"

__license__ = "BSD or Apache License, Version 2.0"
__copyright__ = "Copyright 2013-2017 {0}".format(__author__)
